# Play Framework Cloudron App

This repository contains the Cloudron app package source for [PlayFramework](https://www.playframework.com/).

## Description

Easily host website/service made with PlayFramework on the cloudron with a git workflow.
This app is preconfigured to run with postgresql

## Usage

Simply push your PlayFramework project to https://{your-app-url}/_git/page
The `sbt universal:packageZipTarball` command is used to build your app.

```bash
    $: cd {your-git-repo-dir}
    $: git remote add play-cloudron https://{your-app-url}/_git/page
    $: git push play-cloudron master
```

some play configuration are overriden by the play.conf file at `/app/data/play.conf`

```
    include "application.conf"

    # apply your database evolutions scripts without asking you before
    play.evolutions.db.default.autoApply=true
    play.evolutions.db.default.autoApplyDowns=true

    application.domain="##{APP_DOMAIN}" # your app domain : my-app.my-domain.io
    play.http.secret.key="##PLAY_SECRET" # generated at the first install

    application.ssl=true
    pidfile.path=/dev/null

    # postgresql
    db.default.url="##CLOUDRON_POSTGRESQL_URL"
    db.default.username="##CLOUDRON_POSTGRESQL_USERNAME"
    db.default.password="##CLOUDRON_POSTGRESQL_PASSWORD"
``` 

You can add environement specific configuration to this file. All value added after the line `include "application.conf"` while override whose defined in the application.conf file.


## todo

- wipe git repo after build success . dont realy know if necessary, it may decrease the risk of source code leak (is it?) , downside is that the repo may be heavy tu push each time )
- post install message
- finish index.html with information about how to push and git repo address
- handle nginx fallback to default index.html when no code is pushed 
- use nginx ldap module instead of nodejs 
- create a configuration page on /_admin/ with console and basic confg options
- handle send mail on supervisord events (like app crashed or app successfully build and deployed )
- re-up last build when newer build fail
