FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN mkdir -p /app/code
WORKDIR /app/code

RUN echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list && \
sudo apt-key adv --keyserver hkps://keyserver.ubuntu.com:443 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
apt-get update && \
apt-get install -y sbt openjdk-8-jre-headless openjdk-8-jdk


RUN echo "=> sbt symlink" && \
    mkdir -p /app/data/.sbt && \
    ln -sf /app/data/.sbt /root/.sbt

RUN echo "=> ivy symlink" && \
    mkdir -p /app/data/.ivy2 && \
    ln -sf /app/data/.ivy2 /root/.ivy2

# add nginx config
USER root
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/playframework.conf /etc/nginx/sites-available/playframework
RUN ln -s /etc/nginx/sites-available/playframework /etc/nginx/sites-enabled/playframework

RUN mkdir /run/playframework/

# add default index
COPY index.html /app/code/public/
COPY index.html /app/code/public/_healthcheck/

# add play conf 
ADD play.conf.template /app/code/play.conf.template
RUN ln -s /run/playframework/play.conf /app/code/play.conf

#add sbt builder
COPY sbt-build.sh /app/code/sbt-build.sh
RUN chmod +x /app/code/sbt-build.sh
RUN mkdir -p /run/sbtbuild/
# add node ldap server
RUN mkdir /app/code/ldap/
COPY package.json /app/code/ldap
COPY index.js /app/code/ldap
RUN cd /app/code/ldap/ && \
npm install

# add git hook
COPY pre-receive /app/code/

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/playframework/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/code/

CMD ["/app/code/start.sh"]

EXPOSE 9002
