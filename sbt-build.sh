#!/bin/bash

set -eu

BUILD_PATH="/app/data/build"
WEBSITE_PATH="/app/data/website"

echo "=> Build application..."
# chmod -R +rwx $BUILD_PATH 
cd $BUILD_PATH
# sed -e "~name :=~"
sbt -Djline.terminal=jline.UnsupportedTerminal universal:packageZipTarball

echo "=> Unpack build..."
cd $BUILD_PATH/target/universal
tar -zxvf *.tgz --strip 1 -C $WEBSITE_PATH 
chmod +x $WEBSITE_PATH

cd $WEBSITE_PATH/bin

if ls *.bat 1> /dev/null 2>&1; then
    echo "=> removing unecessary files"
    rm *.bat
fi


if [ -e "start" ]; then
    echo "=> removing old launcher"
    rm "start" || true
fi

echo "=> identifing launcher"
mv * start

echo "=> Run application"
supervisorctl restart sbt