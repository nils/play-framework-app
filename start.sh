#!/bin/bash

set -eu


export REPO_PATH="/app/data/repo.git"
export WEBSITE_PATH="/app/data/website"

# mkdir /run/playframework
if [[ ! -d $REPO_PATH ]]; then
    echo "=> First run, create bare repo"
    mkdir -p $REPO_PATH
    git init --bare $REPO_PATH

    echo "=> Install welcome page"
    rm -rf $WEBSITE_PATH
    mkdir -p $WEBSITE_PATH
    cp /app/code/index.html $WEBSITE_PATH/index.html
fi

# if grep "cloudron-welcome-page" $WEBSITE_PATH/index.html > /dev/null; then
#     echo "=> Update welcome page"
#     sed -e "s,##REPO_URL##,${CLOUDRON_APP_ORIGIN}/_git/page," /app/code/index.html > $WEBSITE_PATH/index.html
# fi
echo "=> generating play secret"
if ! [ -e /app/data/.playsecret ]; then
  dd if=/dev/urandom bs=1 count=1024 2>/dev/null | sha1sum | awk '{ print $1 }' > /app/data/.playsecret
fi
PLAY_SECRET=$(cat /app/data/.playsecret)


echo "=> Ensure git hook 2"
cp /app/code/pre-receive $REPO_PATH/hooks/pre-receive
chmod +x $REPO_PATH/hooks/pre-receive

echo "=> Ensure permissions"
chown cloudron:cloudron -R $REPO_PATH /run /app/data

echo "=> Ensure play configuration"
if ! [ -e /app/data/play.conf ]; then 
  sed -e "s~##APP_DOMAIN~${CLOUDRON_APP_DOMAIN}~g" \
      -e "s/##PLAY_SECRET/${PLAY_SECRET}/g" \
      -e "s~##CLOUDRON_POSTGRESQL_URL~${CLOUDRON_POSTGRESQL_URL}~g" \
      -e "s/##CLOUDRON_POSTGRESQL_USERNAME/${CLOUDRON_POSTGRESQL_USERNAME}/g" \
      -e "s/##CLOUDRON_POSTGRESQL_PASSWORD/${CLOUDRON_POSTGRESQL_PASSWORD}/g" \
      /app/code/play.conf.template > /app/data/play.conf
fi

echo "=> Run server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Playframework

